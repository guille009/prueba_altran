$(document).ready(function() {
    let botton_submit_class = $(".botton_submit");

	botton_submit_class.click(function(e){
        e.preventDefault();
		let form = $(this).closest("form");
		let code = form.find(".request_number").val();
        let url = getUrlFromReqCode(code);
        let data = form.serialize();

        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: url+"?"+data,
        }).done(function(response){
            let result_list = form.find('.result_list');
			result_list.text("");
            if(response.result!="ok")
                return swal('Error',response.msg,'error');
            if(Array.isArray(response.msg)){
                for(let data of response.msg)
                    result_list.append("<li>"+getPrinterData(data)+"</li>")
            }
            else{
                result_list.append("<li>"+getPrinterData(response.msg)+"</li>")
            }
        }).fail(function(){
            swal('Error',"Contact with administration",'error');
        });
    });

	function getPrinterData(data){
		let str = "";
		for(let key in data){
			if(key=="_id" || key=="__v") continue;
			str=str+key+": "+data[key]+"  ";
        }
        return str;
	}

    function getUrlFromReqCode(code){
        switch(code){
            case "1":
                return "/api/userByName";
            case "2":
                return "/api/policiesByUserName";
            case "3":
                return "/api/usersByPolicieNumber";
            default:
                return "/api/userById";
        }
    }

});