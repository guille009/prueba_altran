let express = require('express');
let router = express.Router();
let clients_controller = require("../../controllers/api/clientsController");
let access_control = require("../../controllers/access_control/index");


/* API */
router.get('/userById', clients_controller.userById);
router.get('/userByName', clients_controller.userByName);
router.get('/policiesByUserName', access_control.isSessionAnAdmin, clients_controller.policiesByUserName);
router.get('/usersByPolicieNumber', access_control.isSessionAnAdmin, clients_controller.usersByPolicieNumber);

module.exports = router;