let express = require('express');
let router = express.Router();
let views_controller = require("../controllers/views/viewsController");

/* Views */
router.get('/', views_controller.index);

module.exports = router;
