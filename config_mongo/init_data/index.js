let Clients = require("../models/clients");
let Policies = require("../models/policies");
let init_client = require("./init_clients");
let init_policies = require("./init_policies");

require("../config/connection")
.then(()=>{
    console.log("Exito al conectar con mongodb");
})
.then(()=>{ // eliminamos los dtos de la base de datos
    return Promise.all([Clients.remove({}), Policies.remove({})]);
})
.then(()=>{
    init_client.createInitialClient();
})
.then(()=>{
    init_policies.createInitialPolicies();
})
.catch((error)=>{
    console.log("Error al conectarse con la base de datos");
});