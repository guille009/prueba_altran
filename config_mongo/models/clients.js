let mongoose = require('mongoose');

let Schema = mongoose.Schema;

// esquema de los aviones
let ClientsSchema = new Schema({
    id: {type: String, required: true, index: true},
    name: {type: String, required: true},
    email: {type: String, required: true},
    role: {type: String, enum: ['admin', 'user']},
});

// exportar el modelo
module.exports = mongoose.model('Clients', ClientsSchema);
