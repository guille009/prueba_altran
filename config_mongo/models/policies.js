let mongoose = require('mongoose');

let Schema = mongoose.Schema;

// esquema de los aviones
let PoliciesSchema = new Schema({
    id: {type: String, required: true, index: true},
    amountInsured: {type: Number, required: true},
    inceptionDate: {type: Date, default: Date.now},
    email: {type: String, required: true},
    installmentPayment: {type: Boolean, default: false},
    clientId: {type: String, required: true},
});

// exportar el modelo
module.exports = mongoose.model('Policies', PoliciesSchema);
