var mongoose = require('mongoose');

// creamos la conexion con la base de datos
var mongodb_uri = 'mongodb://127.0.0.1/db_prueba';
//prueba:contra@
mongoose.set('useFindAndModify', false); // findOneAndUpdate usa por defecto
                    // useFindAndModify pero esta esta en desuso
mongoose.set('useNewUrlParser', true);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
module.exports = mongoose.connect(mongodb_uri);
