# README #

* Altran access test
* 0.0.0

* Technologies:
	* NodeJS version 12.16.1
	* MongoDB version 4.2.5
	* Bootstrap 3.3.7
	* Jquery 3.4.1

* Configuration:
	* The first spet is intall the dependencies wit the commando "npm install" inside the project folder 
	* To run the application execute "npm start" and will open the port 3000 to show the application
* Database configuration
	* To set up the application is necesary MongoDB
	* To load the data in the database. To do that we can run "node config_mongo/init_data/index", other option is
		create a commando to do it. This can be configurate in the package.json file like "npm starts"
* How to run tests
	* To run the test, after install the dependencies and load the database, with the command "mocha test\show_data_from_admin" we can run
		the admins test, and also, with "test\show_data_from_user" will contain the other test.
* How is build
	* There are severals layers in the proyect in different part of the stack, we have the Model layer with all the information 
		related to the database wich is the "config_mongo" folder. The Controller layer to process all the logic is in the folder "controllers", for the access
		control of normal and admin users I make a layer inside called "access_controll" to valid it. The operand mode is as follows, a chain of executions
		is carried out until the result is reached, each request first passes access control if necessary and consequently we execute the user's request or not
		. The View layer is in the "public" and "views" folder that contain all that the user see. Also, there are more thing, the most
		important is the Cluster, the cluster allows us to take advantage of 100% of the performance of the machine becasuse NodeJS run in a 
		simple thread. Witch the cluster we use all the processors, but this take sames disvantage like..., if we want have persistance with the 
		sessions ,for example, we can not use the traditional mode and save in memory, because for each hit from the client the cluster will
		select one thread randomly to process. For that we use MongoDB to save the sessions. Other problems that required sockets could be solved using
		Redmine database. Others layers are "node_modules" with the ependencies of the proyect and "test" with the test of the application

Other thing to considerer.
	* To the logs I thought to use winston, but i leave "console" in case some crush
	* Use a reverse proxy to increment the performance of the system with a real server like Nginx
	* All the request are independent and to compare the result of normal users and admins I aggregate a variable role in all petition

