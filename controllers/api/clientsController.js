let Clients = require("../../config_mongo/models/clients");
let Policies = require("../../config_mongo/models/policies");

exports.checkLogin = async function(req, res, next){
    try{
		let client = await Clients.findOne({email: req.body.email});
		if(!client) return res.json({result: "error", msg: "Client not found"});
		return res.json({result: "ok", msg: "Ok"});
    }catch (e){
		res.json({result: "error", msg: "An error has occurred, please try again later"});
    }

};


exports.userById = async function(req, res, next){
    let id = req.query.id;
    try{
        let client = await Clients.findOne({id:id});
        if(!client) return res.json({result: "error", msg: "Client not found"});
        return res.json({result: "ok", msg: client});
    }catch (e){
        res.json({result: "error", msg: "An error has occurred, please try again later"});
    }
};

exports.userByName = async function(req, res, next){
    let name = req.query.name;
    try{
		let client = await Clients.findOne({name: name});
		if(!client) return res.json({result: "error", msg: "Client not found"});
		return res.json({result: "ok", msg: client});
    }catch (e){
        res.json({result: "error", msg: "An error has occurred, please try again later"});
    }
};

exports.policiesByUserName = async function(req, res, next){
    let name = req.query.name;
	try{
        let client = await Clients.findOne({name: name});
        if(!client) return res.json({result: "error", msg: "Client not found"});
        let policies = await Policies.find({clientId: client.id});
        if(!policies) return res.json({result: "error", msg: "Policies not found"});
        if(policies.length===0) return res.json({result: "error", msg: "There are no policies for that user"});
        return res.json({result: "ok", msg: policies});
    }catch (e){
	    res.json({result: "error", msg: "An error has occurred, please try again later"});
    }
};

exports.usersByPolicieNumber = async function(req, res, next){
    let id = req.query.id;
    try{
        let policie = await Policies.findOne({id: id});
        if(!policie) return res.json({result: "error", msg: "Policies not found"});
        let client = await Clients.findOne({id: policie.clientId});
        if(!client) return res.json({result: "error", msg: "Client not found"});
        return res.json({result: "ok", msg: client});
    }catch (e){
        res.json({result: "error", msg: "An error has occurred, please try again later"});
    }
};
