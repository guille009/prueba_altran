
exports.isSessionAnAdmin = function (req, res, next){
    if(req.query.role==="admin"){
        next();
    }
    else{
        return res.json({result: "error", msg: "You do not have permission to get the data"});
    }
};

