let chai = require('chai');
chai.use(require('chai-match'));
let assert   = chai.assert;
let request = require("request-promise");
let util = require("../utils/index");

describe("Admin test", ()=> {

    it("Get user by Id", function(done) {
        request({
            method: 'GET',
            url: util.api_url+"/userById?id=a3b8d425-2b60-4ad7-becc-bedf2ef860bd&role=admin",
            json: true
        }).
        then((body)=>{
            assert.equal(body.result, "ok", "Error in get data from user");
            assert.equal(body.msg.email, "barnettblankenship@quotezart.com", "Error in get data from user");
            done();
        });
    });

    it("Get user by name", function(done) {
        request({
            method: 'GET',
            url: util.api_url+"/userByName?name=Barnett&role=admin",
            json: true
        }).
        then((body)=>{
            assert.equal(body.result, "ok", "Error in get data from user");
            assert.equal(body.msg.email, "barnettblankenship@quotezart.com", "Error in get data from user");
            done();
        });
    });

    it("Get user by invalid user name", function(done) {
		request({
			method: 'GET',
			url: util.api_url+"/userByName?name=Barnetttttt&role=admin",
			json: true
		}).
		then((body)=>{
			assert.equal(body.result==="ok", false, "Error in get data from invalid user");
			done();
		});
    });

    it("Get policies by user name with no policies", function(done) {
		request({
			method: 'GET',
			url: util.api_url+"/policiesByUserName?name=Barnett&role=admin",
			json: true
		}).
		then((body)=>{
			assert.equal(body.result, "error", "Error in get policies from user");
			done();
		});
    });

    it("Get policies by user name with policies", function(done) {
		request({
			method: 'GET',
			url: util.api_url+"/policiesByUserName?name=Britney&role=admin",
			json: true
		}).
		then((body)=>{
			assert.equal(body.result, "ok", "Error in get policies from user");
			done();
		});
    });

    it("Get user by policie id", function(done) {
		request({
			method: 'GET',
			url: util.api_url+"/usersByPolicieNumber?id=56b415d6-53ee-4481-994f-4bffa47b5239&role=admin",
			json: true
		}).
		then((body)=>{
			assert.equal(body.result, "ok", "Error in get policies from user with permission");
			done();
		});
    });

});

